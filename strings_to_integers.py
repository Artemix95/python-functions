
# function that takes a list of strings as an argument and converts it to integers and sums the list.
def convert_add(string_list):
    integer_list = [int(num) for num in string_list]
    total_sum = sum(integer_list)
    return total_sum


def main():
    my_list = ['1', '3', '5']
    result = convert_add(my_list)
    print(result)  # Output: 9


# Entry point of the program
if __name__ == "__main__":
    main()
