# function that takes a lis of strings as an argument. The function
# should check if the list has any duplicates. If there are no duplicates,
# the function should return the duplicates.
# If there are no duplicates, the function should return "No duplicates"

fruits = ["apple", "orange", "banana", "apple", "orange", "banana"]
names = ["Yoda", "Moses", "Joshua", "Mark"]


def check_duplicates(list):
    duplicates = set()
    for item in list:
        counter = list.count(item)
        if counter > 1:
            duplicates.add(item)
    if len(duplicates) == 0:
        return "No duplicates"
    else:
        return duplicates


print(check_duplicates(fruits))
print(check_duplicates(names))
