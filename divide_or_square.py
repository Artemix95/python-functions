# Function that takes one number and returns the square root of the number if
# it is divisible by 5, return its remainder if it is not divisible by 5.
from math import sqrt


def divide_or_square(number):
    remainder = number % 5
    if remainder == 0:
        return "%.2f" % sqrt(number)
    else:
        return remainder


input_number = int(input("Enter a number: "))
print(divide_or_square(input_number))
