# function takes a dictionary as an argument and returns the first
# longest value of the dictionary

dictionary = {
    "middle_name": "Andrea",
    "name": "George",
    "surname": "Blacks",
    "city": "New York"
}


def longest_value(diction):
    longest = list(diction.values())[0]
    for value in diction.values():
        if len(value) > len(longest):
            longest = value
    return longest


print(longest_value(dictionary))
