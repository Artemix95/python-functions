
# function that takes a list of strings as an argument.
# The function should check if the list has any duplicates.
# If there are duplicates, the function should return the duplicates.
def check_duplicates(string_list):
    duplicates = []
    seen = set()
    for string in string_list:
        if string in seen:
            duplicates.append(string)
        else:
            seen.add(string)

    if duplicates:
        return duplicates
    else:
        return "No duplicates"


def main():
    fruits = ['apple', 'orange', 'banana', 'apple']
    fruit_duplicates = check_duplicates(fruits)
    print(fruit_duplicates)  # Output: ['apple']

    names = ['Yoda', 'Moses', 'Joshua', 'Mark']
    name_duplicates = check_duplicates(names)
    print(name_duplicates)  # Output: No duplicates


if __name__ == "__main__":
    main()
