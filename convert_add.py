# function that takes a list of strings as an argument
# and converts it to integers and sums the list


lista = ["1", "3", "5", "1", "10"]


def convert_add(list):
    int_list = [int(i) for i in list]
    return sum(int_list)


print(convert_add(lista))


