# You are given a list of names above. This list is made up of names
# of lowercase and uppercase letters. Your task is to write a code
# that will return a tuple of all the names in the list that have only
# lowercase letters. Your tuple should have names sorted
# alphabetically in descending order.

names = ["kerry", "dickson", "John", "Mary", "carol", "Rose", "adam"]

filtered_names = tuple(sorted([name for name in names if name.islower()], reverse=True))
print(filtered_names)  # Output: ('kerry', 'dickson', 'carol', 'adam')
