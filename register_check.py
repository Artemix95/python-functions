# function that checks how many students are in school.
# The function takes a dictionary as a parameter.
# If the student is in school, the dictionary says ‘yes’.
# If the student is not in school, the dictionary says ‘no’.
# Your function should return the number of students in school.
def register_check(register):
    count = 0
    for status in register.values():
        if status == 'yes':
            count += 1
    return count


def main():
    register = {'Michael': 'yes', 'John': 'no', 'Peter': 'yes', 'Mary': 'yes'}
    num_students_in_school = register_check(register)
    print(num_students_in_school)  # Output: 3


if __name__ == "__main__":
    main()
